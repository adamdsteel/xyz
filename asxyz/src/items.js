var items = [
    {
        "title": "EasyJournal",
        "link":"https://play.google.com/store/apps/details?id=xyz.adamsteel.easyjournal",
        "thumbnail":"easyjournal.png",
        "description":"A simple, straightforward, and private journal app for Android",
        "technologies":"Java / Android Studio / SQLite",
        "completion":"30%",

        "codelink":"https://github.com/adamdsteel/easyjournal",
        "mainlink":"https://play.google.com/store/apps/details?id=xyz.adamsteel.easyjournal",

        "codeimage":"ejcode.png",
        "linkimage":"ejlink.png",
        "iconimage":"ejicon.png", //A square icon for the app/site

        "type":"app",
    },
    {
        "title": "LiveCharm.me",
        "link":"https://livecharm.me",
        "thumbnail":"livecharm.jpg",
        "description":"A small-business retail site - users can upload images and have them printed onto photo pendants",
        "technologies":"JavaScript / Jquery / Bootstrap / HTML5 / Canvas / PHP / Responsive design / PayPal integration",
        "completion":"95%",

        "codelink":"https://bitbucket.org/adamdsteel/livecharm/src",
        "mainlink":"https://livecharm.me",

        "codeimage":"lccode.png",
        "linkimage":"lclink.png",
        "iconimage":"lcicon.png", 

        "type":"site",
    },
    {
        "title": "adamsteel.xyz",
        "link":"https://adamsteel.xyz",
        "thumbnail":"xyz.jpg",
        "description":"This site",
        "technologies":"React / HTML / CSS (SCSS) / JavaScript / Cpanel",
        "completion":"60%",

        "codelink":"https://bitbucket.org/adamdsteel/xyz/src/master/",
        "mainlink":"https://adamsteel.xyz",

        "codeimage":"xyzcode.png",
        "linkimage":"xyzlink.png",
        "iconimage":"xyzicon.png",

        "type":"site",
    },
    {
        "title": "Magic Wand",
        "link":"https://bitbucket.org/adamdsteel/magicwand",
        "thumbnail":"xyz.jpg",
        "description":"A simple augmented reality controller for phones",
        "technologies":"C++, OpenCV, Kotlin",
        "completion":"20%",

        "codelink":"https://bitbucket.org/adamdsteel/magicwand",
        "mainlink":"https://adamsteel.xyz/magicwand.html",

        "codeimage":"wandcode.png",
        "linkimage":"wandlink.png",
        "iconimage":"wandicon.png",

        "type":"details",
    },
    


    ];

export { items };
