import './items.js';
import React from 'react';
import logo from './logo.svg';
import './xyz.scss';
import './App.css';
import { items } from './items.js';


function App() {
  //Insert recursion joke here
  return (
      <div className="App">    
          <span className="devname">Adam Steel &nbsp;&nbsp;</span> <span className="devword"> Developer</span>
          <div className="outer">
          <p className="maintitle">About:</p>
            <div className="metext">
              <p>I consider myself a primarily self-taught developer, but I do hold a degree in game
              design and development from Deakin University.
              <br />This gave me an understanding of real-time systems, 3d environments, and UX considerations.
              </p>
              
              <p>I am highly interested in augmented reality and computer vision, and have been working with the OpenCV open-source computer vision library in my spare time</p>
              <p>I am also interested in gaining more experience working with web development (especially React),
              and Android (native Java\Kotlin and React Native)</p>
          
          <p className="maintitle">Projects:</p>
              <ItemCollection></ItemCollection>
            
        </div>
      </div>
    </div>
  );
}

class ItemLine extends React.Component {
  render(){
    return (
      <div>
        
        <div className="projectbox">
          <div className="icondiv">
            <img src={this.props.item.iconimage} height="190px"></img>
          </div>
          <div className="projectinner">
            <div className="projecttitle">
                  <a href={this.props.item.link} target="_blank">{this.props.item.title}</a>
            </div>
            <div className="projectdescription">{this.props.item.description}</div>
            <div className="technologies">{this.props.item.technologies}</div>

              
            <CompletionBox completion={this.props.item.completion}></CompletionBox>  

          </div>
        </div>
        
        <div className="boxesarea">
          <CodeArea item={this.props.item}></CodeArea>
          <LinkArea item={this.props.item}></LinkArea>
        </div>

        <div> <br /> </div>
      </div>

    );
  }
}


class BottomText extends React.Component {
  render(){
    return(
      <div className="bottomtext"> 
          <span className="boxtext"><a href={this.props.link}>{this.props.text}></a></span>
      </div>
    );
 }
}

class CodeArea extends React.Component {
  render(){
      return( 
      <div className="codebox" style={{backgroundImage: "url(" + this.props.item.codeimage + ")"}}>
        <BottomText text="View code" link={this.props.item.codelink}></BottomText>
      </div>
      );
  }
}

class VideoArea extends React.Component {
  render(){
    return( 
      <div className="videobox">
        <BottomText text="More info" ></BottomText>
      </div>
    );
  }
}

class LinkArea extends React.Component {
  render(){
    return( 
    <div className="linkbox" style={{backgroundImage: "url(" + this.props.item.linkimage + ")"}}>
      <BottomText text={"View  " + this.props.item.type} link={this.props.item.mainlink}></BottomText>
    </div>
    );
  }
}

class ItemCollection extends React.Component {
  render(){
      return(
        <div>
          {
            items.map(item => (
              <ItemLine key={item.title} item={item}></ItemLine>
               
            ))}
        </div>
      );
    }
}

class CompletionBox extends React.Component {
  render(){
      return(
        <div class="completionouter">
          Completion: <br />
          <div className="completionbox"> 
          
            <div className="completioninner" style={{width: this.props.completion}}></div>
            
          </div>
        </div>
      );
  }
}

export default App;
